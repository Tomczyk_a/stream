package homework.next;

public class Processor {
    public void execute(Executor executor) {
        executor.process();
    }
}
