package homework;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BoardGame {

    public String name;
    public Double rating;

    public void setName(String name) {
        this.name = name;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public Double getRating() {
        return rating;
    }

    public static List<BoardGame> getGAMES() {
        return GAMES;
    }

    public BoardGame(String name, double rating) {
        this.name = name;
        this.rating = rating;
    }

    public final static List<BoardGame> GAMES = new ArrayList<>();

    public static void fillList() {
        GAMES.add(new BoardGame("John Smith", 5));
        GAMES.add(new BoardGame("Dorothy Newman", 4));
        GAMES.add(new BoardGame("John Wolkowitz", 9));
        GAMES.add(new BoardGame("Lucy Riley", 2));
        GAMES.add(new BoardGame("Owen Rogers", 7));
        GAMES.add(new BoardGame("Matilda Davies", 3));
        GAMES.add(new BoardGame("Declan Booth", 5));
        GAMES.add(new BoardGame("Corinne Foster", 7));
        GAMES.add(new BoardGame("Khloe fry", 6));
        GAMES.add(new BoardGame("Martin Valenzuela", 1));
    }

    public static void main(String[] args) {
        fillList();
        double highestRanking = 0;
        BoardGame bestGame = null;
        for (BoardGame game : BoardGame.GAMES) {
            if (game.name.contains("a")) {
                if (game.rating > highestRanking) {
                    highestRanking = game.rating;
                    bestGame = game;
                }
            }
        }
        System.out.println(bestGame.name);

        final BoardGame a = BoardGame.GAMES.stream()
                .filter(boardGame -> boardGame.name.contains("a"))
                .reduce((boardGame, boardGame2) -> (boardGame.rating.compareTo(boardGame2.rating) >= 0) ? boardGame : boardGame2)
                .orElseThrow(RuntimeException::new);
        System.out.println(a);

        final Double result = BoardGame.GAMES.stream()
                .map(BoardGame::getRating)
                .reduce((aDouble, aDouble2) -> aDouble + aDouble2)
                .orElseThrow(RuntimeException::new);

        System.out.println(result);


//                .reduce((boardGame, boardGame2) -> (boardGame.rating+boardGame2.rating))

//                .peek(System.out::println)
//                .collect(Collectors.toList());

        //a.forEach(System.out::println);
    }

    @Override
    public String toString() {
        return "BoardGame{" +
                "name='" + name + '\'' +
                ", rating=" + rating +
                '}';
    }
}
