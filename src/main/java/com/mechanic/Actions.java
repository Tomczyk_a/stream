package com.mechanic;


import java.math.BigDecimal;

public class Actions {
     private String idAction;
     private String nameOfAction;
     private BigDecimal payCheck;

    public Actions(String idAction, String nameOfAction, BigDecimal payCheck) {
        this.idAction = idAction;
        this.nameOfAction = nameOfAction;
        this.payCheck = payCheck;
    }

    public String getIdAction() {
        return idAction;
    }

    public String getNameOfAction() {
        return nameOfAction;
    }

    public BigDecimal getPayCheck() {
        return payCheck;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Actions)) return false;

        Actions actions = (Actions) o;

        if (!getIdAction().equals(actions.getIdAction())) return false;
        if (!getNameOfAction().equals(actions.getNameOfAction())) return false;
        return getPayCheck().equals(actions.getPayCheck());
    }

    @Override
    public int hashCode() {
        int result = getIdAction().hashCode();
        result = 31 * result + getNameOfAction().hashCode();
        result = 31 * result + getPayCheck().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Actions{" +
                "idAction='" + idAction + '\'' +
                ", nameOfAction='" + nameOfAction + '\'' +
                ", payCheck=" + payCheck +
                '}';
    }
}
