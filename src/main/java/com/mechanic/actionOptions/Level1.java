package com.mechanic.actionOptions;

import java.math.BigDecimal;

public class Level1 implements Counter {
    private final Counter counter;

    public Level1(Counter counter) {
        this.counter = counter;
    }

    @Override
    public BigDecimal getCost() {
        return counter.getCost();
    }

    @Override
    public String getDescription() {
        return counter.getDescription();
    }
}
