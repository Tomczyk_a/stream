package com.mechanic.actionOptions;

import java.math.BigDecimal;

public class Basic implements Counter {
    @Override
    public BigDecimal getCost() {
        return new BigDecimal(150);
    }

    @Override
    public String getDescription() {
        return "15 Basic car elements";
    }
}
