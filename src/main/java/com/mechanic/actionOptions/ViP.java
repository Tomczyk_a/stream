package com.mechanic.actionOptions;

import java.math.BigDecimal;

public class ViP extends AbstractLevel {
    public ViP(Counter counter){
        super(counter);
    }

    @Override
    public BigDecimal getCost() {
        return super.getCost().add(new BigDecimal(200));
    }

    @Override
    public String getDescription() {
        return super.getDescription()+ "+ 30 points";
    }
}
