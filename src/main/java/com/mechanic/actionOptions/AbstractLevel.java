package com.mechanic.actionOptions;

import java.math.BigDecimal;

public class AbstractLevel implements Counter {
    private final Counter counter;

    public AbstractLevel(Counter counter) {
        this.counter = counter;
    }

    @Override
    public BigDecimal getCost() {
        return counter.getCost();
    }

    @Override
    public String getDescription() {
        return counter.getDescription();
    }
}
