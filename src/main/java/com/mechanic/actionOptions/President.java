package com.mechanic.actionOptions;

import java.math.BigDecimal;

public class President extends AbstractLevel{
    public President(Counter counter) {
        super(counter);
    }
    public BigDecimal getCost() {
        return super.getCost().add(new BigDecimal(600));
    }

    @Override
    public String getDescription() {
        return super.getDescription()+ "+ cleaning " +
                "+ 45 points ";
    }
}
