package com.mechanic.actionOptions;

import java.math.BigDecimal;

public interface Counter {
    BigDecimal getCost();
    String getDescription();

}
