package com.mechanic;

import javax.sql.rowset.serial.SerialStruct;
import java.math.BigDecimal;
import java.util.List;

public interface PaymentProcessor {

   BigDecimal calculatePayment(List<Actions> actions);
}
